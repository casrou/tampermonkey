// ==UserScript==
// @name         Browser: Show shard of account
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  try to take over the world!
// @author       Casper
// @match        https://browser.partisiablockchain.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=partisiablockchain.com
// @grant        none
// ==/UserScript==

const ACCOUNT_OR_CONTRACT_RE = /(?:contracts|accounts)\/(\w+)/;
const SHARDS = ["Shard 0", "Shard 1", "Shard 2"];

function shardForAddress(address) {
    // Create a buffer
    var buffer = new Uint8Array(address.match(/../g).map(h => parseInt(h, 16))).buffer

    // Create a data view of it
    var view = new DataView(buffer);

    const shardIndex = Math.abs(view.getInt32(17)) % SHARDS.length;
    return SHARDS[shardIndex];
}


function showShard(path) {
    const result = path.match(ACCOUNT_OR_CONTRACT_RE);
    if (result) {
        console.log(path);
        const address = result[1];
        console.log("address", address);
        const shard = shardForAddress(address);
        console.log("shard", shard);

        let root = document.getElementsByClassName("MuiBox-root")[5];
        let shardText = document.getElementById("shard");
        if (!shardText) {
            shardText = document.createElement("div");
            root.prepend(shardText);
        }
        shardText.innerText = shard;
        shardText.id = "shard";
        shardText.className = "css-whiipr";
        shardText.style = "margin: 20px 0 -20px 40px";
    } else {
        document.getElementById("shard")?.remove();
    }
}

(function() {
    'use strict';

    var rs = history.pushState;
    history.pushState = function(){
        rs.apply(history, arguments); // preserve normal functionality
        showShard(arguments[2]); // do something extra here
    };

    addEventListener("popstate", (event) => {
        showShard(location.pathname);
    });

    showShard(location.pathname);
})();